all: multigpu_rccl

multigpu_rccl: multigpu_rccl.cc
	hipcc multigpu_rccl.cc -o multigpu_rccl -L/opt/rocm/lib -lrccl

clean:
	rm multigpu_rccl